import React, { Component } from "react";

import Weather from "./component/Weather";
import Searchbar from "./component/Searchbar";

import "weather-icons/css/weather-icons.css";
import "./css/weather.scss";
import pic from "./asset/icons/weather.jpg";

const API_key = "c2d52654d0ef9c8ab428fd78c5fe519f";
export default class App extends Component {
  state = {
    error: false,
    city: null,
    country: null,
    temp: null,
    minTemp: null,
    maxTemp: null,
    icon: null,
    description: "",
  };

  componentDidMount() {
    // this.getWeather();
    this.weatherIcon = {
      Drizzle: "wi-storms",
      Thunderstorm: "wi-sleet",
      Rain: "wi-showers",
      Show: "wi-show",
      Atmosphore: "wi-flog",
      Clear: "wi-day-sunny",
      Clouds: "wi-day-fog",
    };
  }

  celsius = (temp) => {
    var cell = Math.floor(temp - 273.15);
    return cell;
  };
  get_weatherIcons = (icons, rangeId) => {
    switch (true) {
      case rangeId >= 200 && rangeId < 232:
        this.setState({ icon: icons.Thunderstorm });
        break;
      case rangeId >= 300 && rangeId <= 321:
        this.setState({ icon: icons.Drizzle });
        break;
      case rangeId >= 500 && rangeId <= 521:
        this.setState({ icon: icons.Rain });
        break;
      case rangeId >= 600 && rangeId <= 622:
        this.setState({ icon: icons.Snow });
        break;
      case rangeId >= 701 && rangeId <= 781:
        this.setState({ icon: icons.Atmosphere });
        break;
      case rangeId === 800:
        this.setState({ icon: icons.Clear });
        break;
      case rangeId >= 801 && rangeId <= 804:
        this.setState({ icon: icons.Clouds });
        break;

      default:
        break;
    }
  };

  getWeather = async (e) => {
    e.preventDefault();

    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    if (city && country) {
      var json = [];

      await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_key}`
      ).then((response) => {
        json = response.json();
      });

      json.then((res) => {
        console.log(res);
        this.setState({
          city: `${res.name},${res.sys.country}`,
          // country: res.sys.country,
          temp: this.celsius(res.main.temp),
          minTemp: this.celsius(res.main.temp_min),
          maxTemp: this.celsius(res.main.temp_max),
          // icon: this.weatherIcon.thunderstorm,
          description: res.weather[0].description,
        });
        this.get_weatherIcons(this.weatherIcon, res.weather[0].id);
      });
    } else {
      this.setState({ error: true });
    }
  };
  render() {
    return (
      <div styles={{ backgroundImage: `url(${pic})` }}>
        <Searchbar loadWeather={this.getWeather} error={this.state.error} />
        <Weather
          city={this.state.city}
          country={this.state.country}
          temp={this.state.temp}
          minTemp={this.state.minTemp}
          maxTemp={this.state.maxTemp}
          weatherIcon={this.state.icon}
          description={this.state.description}
        ></Weather>
      </div>
    );
  }
}
