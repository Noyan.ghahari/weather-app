import React, { Component } from "react";

import "../css/weather.scss";

export default class Searchbar extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="searchbar-container searchbar-container-mob">
          <form className="form" onSubmit={this.props.loadWeather}>
            <div className="country-search">
              <input
                className="country-input country-input-mob"
                type="text"
                name="country"
                autoComplete="off"
                placeholder="country"
              />
            </div>
            <div className="city-search">
              <input
                className="city-input city-input-mob"
                type="text"
                name="city"
                autoComplete="off"
                placeholder="city"
              />
            </div>
            <div className="button-cont">
              <button className="getweather-button getweather-button-mob">
                Get Weather
              </button>
            </div>
          </form>
          <div>{this.props.error ? error() : null}</div>
        </div>
      </React.Fragment>
    );

    function error() {
      return (
        <div className="error-alert-cont" role="alert">
          <h2 className="error-alert-text error-alert-text-mob">
            Please enter city or country...!
          </h2>
        </div>
      );
    }
  }
}
