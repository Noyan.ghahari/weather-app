import React, { Component } from "react";
import "../css/weather.scss";
export default class Wather extends Component {
  render() {
    function minMaxTemp(max, min) {
      if (min && max) {
        return (
          <div className="min-max-cont min-max-cont-mob">
            <span className="min-deg">{min}&deg;</span>
            <span className="max-deg">{max}&deg;</span>
          </div>
        );
      } else {
        return null;
      }
    }
    return (
      <React.Fragment>
        <div className="container container-mob">
          <div className="country-city-cont">
            <h1>{this.props.city}</h1>
          </div>

          <h2 className="weather-icon">
            <i className={`wi ${this.props.weatherIcon}`}></i>
          </h2>
          {this.props.temp ? (
            <h2 className="degre-icon">{this.props.temp}&deg;</h2>
          ) : null}
          {minMaxTemp(this.props.minTemp, this.props.maxTemp)}
          <div className="desc-cont-style desc-cont-style-mob">
            <h2>{this.props.description}</h2>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
